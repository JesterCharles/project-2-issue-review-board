# Project 2 Storyboard

| As A           | I want to                           | So that                                                           | Points | Status |
| -------------- | ----------------------------------- | ----------------------------------------------------------------- | ------ | ------ |
| Constituent    | Submit an issue                     | the review board can address the issue                            | 1      | LIVE   |
| Constituent    | View Town Hall Meetings and details | I can attend meetings important to me                             | 2      | LIVE   |
| Council Member | Review all issues                   | no one's issues are missed                                        | 3      | LIVE   |
| Council Member | Review issues by type               | the most prevalent issues are addressed first                     | 2      | LIVE   |
| Council Member | Mark issue as reviewed              | to filter out issues that will not be addressed or solved already | 2      | LIVE   |
| Council Member | Highlight an issue                  | Other council members see it                                      | 3      | LIVE   |
| Council Member | Create Town Meetings                | constituents can air their thoughts                               | 2      | LIVE   |
| Council Member | Edit Meetings                       | I can change time/topics to best reflect what is being covered    | 1      | LIVE   |
