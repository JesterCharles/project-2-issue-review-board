# 9/22 Stand up Group 5

## Michelle

    - Goals: Interested in figuring out how to divide up and working understanding git.

## Josh

    - Goals: Start working on town meeting scheduler and make it pair with the front end.

## Rafael

    - Goals: Figuring out how to work together as a team. Curious to see how it integrates as a team.

## Charles

    - Goals: Maybe break down the components of the backend to work together on while pushing to git. Interested in seeing how coding together works.

# Overall

    - Goals for day1: Working together to build the town-meeting scheduler for both backend and frontend.
    - Challenges: See how quickly we get backend done.
    - Breakdown: Building out the skeleton of the frontend and backend communicating. Nothing fancy, CSS can wait until later.

# 9/23 Stand up Group 5

## Michelle

    - Current: Code out the publisher and have that be able to handle constituents submitting issues. Was able to put towards the gitlab.
    - Goals: Learn more of terraform and work on frontend.

## Josh

    - Current: Submitted the basic subscriber and added express implementation and pushed all to gitlab.
    - Errors: Getting the array of submitted issues potentially look into timing out or removing
    - Goals: Work on finalizing the subscriber

## Rafael

    - Current: Containerized the docker files for both publisher and subscriber, building of images. Deployed to GCR and assisted with terraform script.
    - Goals: Work on frontend with michelle on pubsub

## Charles

    - Current: Handled a bunch merges, built skeleton of pubsub and finalized the terraform.
    - Goals: Build the skeleton for the SQL townhall meeting scheduler

# Overall

    - Goals for today: Finalize the pubsub in its entirety and at least start working on SQL backend.
    - Challenges: See how quickly we get backend done after the skeleton.

# 9/24 Stand up Group 5

## Michelle

    - Current: Finished the basic react component to submit the issue to the publisher and submitted it.
    - Goals: Work on a frontend for datastore/issue-analysis and contacting the subscriber to activate.

## Josh

    - Current: Streamlined the subscriber. Finished all but the issue analysis routes, except the report
    - Goals: Will finalize the issue report service

## Rafael

    - Current: Created react app with all dependencies and the welcome page. Just needs to fix a few lines and then will submit.
    - Goals: start working some of the sql backend

## Charles

    - Current: Updated terraform to include the appropriate service accounts for the subscriber cloud run. Built the skeleton built for the SQL.
    - Discussion Topic: Talk about discussion topics
    - Goals: Implement Kubernetes into our issue-analysis service and work on the testing and CI/CD for our SQL instance

# Overall

    - Goals for today: Enjoy our friday.
    - Challenges: Get the issue-analysis completed and at least a decent a chunk of the sql instance.

# 9/27 Stand up Group 5

## Michelle

    - Current: Worked on getting the issue-analysis frontend and close to submitting a merge request.
    - Goals: Finalize the issue-analysis frontend service.

## Josh

    - Current: Finalized the issue-analysis, just waiting on the PVC.
    - Goals: Might work on the SQL instance if still waiting on PVC.

## Rafael

    - Current: Able to submit the SQL DAOs for our scheduler service
    - Goals: Either frontend for the scheduler or do the testing for scheduler.

## Charles

    - Current: Terraform script was officially finalized. Assisted with issue-analysis service and reviewed the merge request.
    - Goals: Want to finish the kubernetes and get the testing done for the SQL instance and CI/CD for gitlab.

# Overall

    - Goals for today: Finalize the backend for the scheduler and issue-analysis service(if he goes PVC).
    - Challenges: Finalize all the core components by wednesday

# 9/28 Stand up Group 5

## Michelle

    - Current: Finalized the front-end issue analysis portion, wants to iron out details with filtering out the analysis and how we might re-render.
    - Goals: Wants to go over react and some backend.

## Josh

    - Current: Finished town scheduler backend for index and services.
    - Goals: Work on frontend for scheduler, might make skeleton.

## Rafael

    - Current: Made a few changes to the overall aesthetic of the frontend.
    - Goals: Work on authorization and take over for josh when he interviews and has doc appointment.

## Charles

    - Current: Work on getting the PCV to work with the files and began kubernetes deployment. Finalzied the tessting and CI/CD pipeline works now fro the gitlab scheduler.
    - Goals: Finalize the kubernetes and bigquery section. Make sure to go over as group so everyone understands and can ask questions.

# Overall

    - Goals for today: Finalize the backend portion.
    - Challenges: Get evreything done tomorrow and work on polishing everything up.

# 9/30 Stand up Group 5

## Michelle

    - Current: Spent time working along side Charles with understanding Kubernetes. A lot of time messing with the view issues page and playing with hooks and diving deep into understanding React
    - Goals: Go through questions about kubernetes, finish up the react frontend for issue-analysis

## Josh

    - Current: Mainly worked on the frontend for the meeting services mainly done.
    - Goals: Just needs to implement the delete and update for frontend and will then be completed.

## Rafael

    - Current: Working along side charles with understanding Kubernetes, working on the authorization and looking at best practices. Cleaned up from React end.
    - Goals: Implement authorization at base level and attempt playing with JWT

## Charles

    - Current: I went over kubernetes as a whole for deploying our backend (excluding pub/sub). Able deploy all services to load balancers initially and eventually set up using ClusterIPs and ingress.
    - Goals: Discuss and implement ingress authorization with Rafa and go over with group

# Overall

    - Goals for today: Iron out everything that's running on the backend and frontend.
    - Challenges: Make it look pretty and work on each member being able to present on all aspects of our project.

# 10/1 Stand up Group 5

## Michelle

    - Current: Added CSS on the components she worked on. Spending A LOT of time and fun with the css bootstrap/material-ui. Makes sure everything is properly functional
    - Goals: Clean things up and do a final push for the frontend and make sure it's all working.

## Josh

    - Current: Finished the routes for the meeting scheduler for the frontend and waiting on the implementation .
    - Goals: cleaning it up so everything is nice and convenient.

## Rafael

    - Current: Working on making the front end function with the authorization tokens, testing phases of taking the token to allow access to certain pages..
    - Goals: Compile the frontend from everyone and make sure the certain pages route appropriately and utilize the token to grant access to certain fields

## Charles

    - Current: Create and incorporating the authorization backend service into kubernetes.
    - Goals: Go over the authorization token being passed into the frontend and ensure the frontend is entirely incorporated with minimal bugs.

# Overall

    - Goals for today: Last hour or two go through and make sure everything is functional.
    - Challenges: Everyone talking about each section project 2 and enjoy the weekend!
