# Project-2-scrum

# Town-Hall Issue Review Board

## **Two week sprint due Oct 5th**

### Brief Overview

-   Our objective is to allow constituents the means to submit issues to our council members and allow the council the opportunity to review and address these issues by scheduling town meetings.

### Overview of our Entities

-   Issues submission form

    -   Date of posting
    -   Date of issue
    -   Issue Description
    -   Location
    -   Type of Issue
        -   Infrastructure
        -   Safety
        -   Public Health
        -   Pollution
        -   Noise/Disturbing the peace
        -   Other

-   Issues for council members include everything above

    -   Highlighting for priority
    -   Marking reviewed
    -   Council Member who reviewed

-   Town meetings
    -   Meetings at a minimum should have the following information
        -   Location
            -   Address
            -   Room
        -   Time
        -   Topics of discussion
        -   Council Member in-charge (potentially)
