const express = require("express");
const jwt = require("jsonwebtoken");
const cors = require("cors");
const middleware = require("./middleware");
const { Datastore } = require("@google-cloud/datastore");

const datastore = new Datastore();
const app = express();

app.use(express.json());
app.use(cors());

app.post("/auth", async (req, res) => {
	const body = req.body;
	const query = datastore.createQuery("user").filter("password", "=", body.password).filter("email", "=", body.email);
	const [authUser] = await datastore.runQuery(query);
	console.log(authUser);
	if (authUser[0] === undefined) {
		res.status(409);
		res.send(`Authentication failed, email & password incorrect or not found`);
	} else {
		const token_payload = {
			name: authUser[0].email,
			password: authUser[0].password
		};
		let token = jwt.sign(token_payload, "jwt_secret_password", { expiresIn: "2h" });
		const userJWT = {
			message: "Token Created for Authorized User",
			token: token
		};
		res.status(200);
		res.send(userJWT);
	}
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Authorization has start"));
