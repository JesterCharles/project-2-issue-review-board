# meeting-authorization

## If you wish to deploy this project to kubernetes submit the following commands **IN ORDER**

```bash
# SKIP IF you've already run this helm command and yaml, if done within your project no need to repeat
helm install ingress ingress-nginx/ingress-nginx
kubectl apply -f ingress.yaml

# Everything else to build this particular service
kubectl apply -f auth-sa.yaml
kubectl apply -f deployment.yaml
kubectl apply -f service-auth.yaml

```
