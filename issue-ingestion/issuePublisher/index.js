const { PubSub } = require("@google-cloud/pubsub");
const bunyan = require("bunyan");
const pubsub = new PubSub({ projectId: "townhall-group5" });
const { LoggingBunyan } = require("@google-cloud/logging-bunyan");

const cloudLogger = new LoggingBunyan();
const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const config = {
	name: "issue-submission",
	streams: [{ stream: process.stdout, level: "info" }, { path: "signups.log", type: "file" }, cloudLogger.stream("info")]
};

const logger = bunyan.createLogger(config);

app.post("/submitissue", (req, res) => {
	const issue = req.body;
	pubsub.topic("issue-topic").publishJSON(issue);
	logger.info("A new issue has been submitted" + JSON.stringify(issue));
	res.status(201);
	res.send("Issue has been submitted");
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Application Started"));
