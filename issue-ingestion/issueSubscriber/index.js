const { PubSub } = require("@google-cloud/pubsub");
const express = require("express");
const cors = require("cors");
const {Datastore} = require('@google-cloud/datastore')

const pubsub = new PubSub({projectId:'townhall-group5'});
const datastore = new Datastore();

const app = express();
app.use(express.json());
app.use(cors());

app.get("/", (req,res) =>{
    const subscription = pubsub.subscription('issue-subscription');

    subscription.on('message', (message) =>{
        const key = datastore.key(['issue']);
        datastore.save({key:key,data:JSON.parse(message.data.toString())})
        console.log(JSON.parse(message.data.toString()));
        message.ack();
    })
})

const PORT = process.env.PORT || 3000
app.listen(PORT, ()=>console.log('Subscriber Application Started'))