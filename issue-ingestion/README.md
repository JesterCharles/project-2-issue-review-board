# issue-ingestion

Utilize pubsub to allow constituents the ability to submit issues into a topic and allow council members the ability to analyze this data.

# Important gcloud commands for terraform permissions in IAM

-   These commands offer use the ability to run

```bash
#User Account
gcloud projects add-iam-policy-binding PROJECT_ID --member='user:someone@gmail.com' --role='roles/iam.serviceAccountUser'

#Service Account
gcloud projects add-iam-policy-binding PROJECT_ID --member='serviceAccount:myserviceaccount@PROJECT_ID.iam.gserviceaccount.com' --role='roles/iam.serviceAccountUser'
```
