provider "google" {
    project = "townhall-group5"
    #Add path to SA JSON key file from IAM for the Pub-Sub account
    credentials = "E:\\interwebDownloads\\townhall-group5-d188a84cd46a.json"

}
provider "google" {
    project = "townhall-group5"
    alias = "runner"
    #Add path to SA JSON key file from IAM for the Cloud Run Account
    credentials = "E:\\interwebDownloads\\townhall-group5-4efb0ade39c3.json"

}



resource "google_pubsub_topic" "issue" {
    name = "issue-topic"

}

resource "google_pubsub_subscription" "issue" {
  name = "issue-subscription"
  topic = google_pubsub_topic.issue.name
   
  ack_deadline_seconds = 100

  enable_message_ordering = false
}

resource "google_cloud_run_service" "issue_publisher" {
    name = "issue-pub-run"
    provider = google.runner
    location = "us-central1"


    template{
        spec{
            service_account_name = "pubsub-admin@townhall-group5.iam.gserviceaccount.com"
            containers{
                image = "gcr.io/townhall-group5/issue-publisher:1.2"
            resources {
                 limits = {
                    cpu = "1.0"
                    memory = "256Mi"
                    }
                }
            }
        }
         metadata {
            annotations = {
            "autoscaling.knative.dev/maxScale" = "5"
            }
        }
    }
   
  traffic{
      percent = 100
      latest_revision = true
  }
}
resource "google_cloud_run_service" "issue_subscriber" {
    name = "issue-sub-run"
    location = "us-central1"
    provider = google.runner
 

    template{
        spec{
            service_account_name = "datastore-subscriber@townhall-group5.iam.gserviceaccount.com"
            containers{
                image = "gcr.io/townhall-group5/issue-subscriber:1.3"
            resources {
                 limits = {
                    cpu = "1.0"
                    memory = "256Mi"
                    }
                }
            }
        }
        metadata {
            annotations = {
            "autoscaling.knative.dev/maxScale" = "5"
            }
        }   
    }
    
  traffic{
      percent = 100
      latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth-pub" {
  provider    = google.runner
  location    = google_cloud_run_service.issue_publisher.location
  project     = google_cloud_run_service.issue_publisher.project
  service     = google_cloud_run_service.issue_publisher.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
resource "google_cloud_run_service_iam_policy" "noauth-sub" {
  provider    = google.runner
  location    = google_cloud_run_service.issue_subscriber.location
  project     = google_cloud_run_service.issue_subscriber.project
  service     = google_cloud_run_service.issue_subscriber.name

  policy_data = data.google_iam_policy.noauth.policy_data
}