const { Datastore, Key } = require("@google-cloud/datastore");
const express = require("express");
const axios = require("axios");
const cors = require("cors");
const { readFile, writeFile } = require("fs").promises;

const app = express();
app.use(express.json());
app.use(cors());

const datastore = new Datastore();

app.get("/issues", async (req, res) => {
	const type = req.query.type;
	const datePosted = req.query.dateposted;
	const dateOfIssue = req.query.dateofissue;
	const reviewed = req.query.reviewed;
	const highlighted = req.query.highlighted;

	if (type) {
		const query = datastore.createQuery("issue").filter("issueType", "=", type);
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});
		if (data[0]) {
			res.send(dataKey);
		} else {
			res.status(404);
			res.send("No issues with that type");
		}
	} else if (datePosted) {
		const query = datastore.createQuery("issue");
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});
		const resArray = dataKey.filter((i) => i.datePosted.includes(datePosted));
		if (data[0]) {
			res.send(resArray);
		} else {
			res.status(404);
			res.send("No issues with that date posted");
		}
	} else if (dateOfIssue) {
		const query = datastore.createQuery("issue").filter("dateOfIssue", "=", dateOfIssue);
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});
		if (data[0]) {
			res.send(dataKey);
		} else {
			res.status(404);
			res.send("No issues with that issue date");
		}
	} else if (reviewed !== undefined) {
		const query = datastore.createQuery("issue").filter("reviewed", "=", reviewed === "true");
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});
		if (data[0]) {
			res.send(dataKey);
		} else {
			res.status(404);
			res.send(`No issues with ${reviewed} reviewed state`);
		}
	} else if (highlighted !== undefined) {
		const query = datastore.createQuery("issue").filter("highlighted", "=", highlighted === "true");
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});
		if (data[0]) {
			res.send(dataKey);
		} else {
			res.status(404);
			res.send(`No issues with ${highlighted} highlighted state`);
		}
	} else {
		const query = datastore.createQuery("issue");
		const [data] = await datastore.runQuery(query);
		const dataKey = data.map((m) => {
			m.id = m[datastore.KEY].id;
			return m;
		});

		res.send(dataKey);
	}
});

app.get("/issues/report", async (req, res) => {
	let weekPast = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000).toISOString();
	let past24 = new Date(Date.now() - 24 * 60 * 60 * 1000).toISOString();
	let reviewCount = 0;
	let notReviewedCount = 0;
	let typeCount = "{";
	let dayTypeCount = "";
	let weekTypeCount = "";

	const query = datastore.createQuery("issue");
	const [data] = await datastore.runQuery(query);

	const allTypes = [
		{ type: "Infrastructure", count: 0, weekCount: 0, dayCount: 0 },
		{ type: "Safety", count: 0, weekCount: 0, dayCount: 0 },
		{ type: "Public Health", count: 0, weekCount: 0, dayCount: 0 },
		{ type: "Pollution", count: 0, weekCount: 0, dayCount: 0 },
		{ type: "Disturbance", count: 0, weekCount: 0, dayCount: 0 },
		{ type: "Other", count: 0, weekCount: 0, dayCount: 0 }
	];
	allTypes.map((t) => {
		data.filter((i) => i.issueType === t.type).map((i) => {
			t.count++;
			if (i.datePosted > weekPast) {
				t.weekCount++;
			}
			if (i.datePosted > past24) {
				t.dayCount++;
			}
			if (i.reviewed) {
				reviewCount++;
			} else {
				notReviewedCount++;
			}
		});
	});

	allTypes.map((t) => {
		typeCount = typeCount + `"${t.type.replace(/\s/g, "")}Count": ${t.count}, `;
		dayTypeCount += `"${t.type.replace(/\s/g, "")}DayCount": ${t.dayCount}, `;
		weekTypeCount += `"${t.type.replace(/\s/g, "")}WeekCount": ${t.weekCount}, `;
	});
	const finalJSON =
		typeCount + dayTypeCount + weekTypeCount + `"totalReviewed": ${reviewCount}, "totalPendingReview": ${notReviewedCount}}`;
	await writeFile("./logs/report.txt", finalJSON);
	res.send(finalJSON);
});

app.put("/issues/:iId", async (req, res) => {
	const body = req.body;
	const key = datastore.key(["issue", Number(req.params.iId)]);
	await datastore.merge({ key: key, data: body });
	res.send("Successfully updated issue");
});

app.get("/issues/lastreport", async (req, res) => {
	const fileData = await readFile("./logs/report.txt");
	const info = fileData.toString();
	res.send(info);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Issue-Analysis Service Started"));
