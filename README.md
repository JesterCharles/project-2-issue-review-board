# Issue Review Board

## Project Description

The Issue Review board is an application for a small local town. The application allows constituents to submit issues that they have noticed in the town. The review board members can look over these issues. Board members can then create meetings which can be viewed by constituents and display information on what needs to be discussed. The application is a microservices architecture that is entirely container based. The application ingests information from GCP pub/sub and uses GKE and Cloud Run for hosting the services.

## Technologies Used

* Node - version 14.17
* Kubernetes
* Google Kubernetes Engine
* GCP Pub/Sub
* Cloud Run
* Google Container Registry
* Docker
* Firebase
* React
* PostgreSQL
* Datastore
* Cloud logging
* Big Query

## Features

List of features ready and TODOs for future development
* Issue ingestion service that allows for anonmyous submission of issues by the towns consistuents.
* An Issue Analysis service for council memebers to review and highlight important town issues.
* A meeting scheduler service to schedule meetings and the topics that will be discussed during them. Scheduled meetings viewbale and filterable by constituents, but only council members have access to create, update or delete them.
* An additional service that utilized JSON Web Tokens for authorization to access the issue analysis page and metting scheduling

To-do list:
* Hash passwords and include a more secure JWT application.

## Contributors

* Josh Forcier
* Michelle Pierce
* Rafael Mora
* Charles Jester
