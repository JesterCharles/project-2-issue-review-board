import axios from "axios";
import { useState } from "react";
import IssueTable from "./issue-table";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Button from "@mui/material/Button";
import { Container } from "react-bootstrap";
import { TextField } from "@material-ui/core";
import Typography from "@mui/material/Typography";
import { useEffect } from "react";

export default function IssueAnalysis() {
	const urlIA = "http://35.232.3.28/ia/issues";
	const [issues, setIssues] = useState([]);
	const [filterType, setFilterType] = useState("");
	const [filterValue, setFilterValue] = useState("true");

	useEffect(() => {
		axios.get("https://issue-sub-run-jjer53nvda-uc.a.run.app/");
	}, []);

	const clearState = () => {
		setFilterType("");
		setFilterValue("true");
	};

	async function getIssues(event) {
		const response = await axios.get(`${urlIA}`);
		setIssues(response.data);
	}

	async function getFilteredIssues() {
		const response = await axios.get(`${urlIA}?${filterType}=${filterValue}`);
		setIssues(response.data);
		clearState();
	}

	async function generateReport() {
		const response = await axios.get(`${urlIA}/report`);
		alert("Report Generated");
	}

	function inputDisplay() {
		let inputDisplay = null;
		if (filterType === "type") {
			inputDisplay = (
				<Box sx={{ minWidth: 120, p: 1 }}>
					<FormControl fullWidth sx={{ p: 1 }}>
						<InputLabel id="demo-simple-select-label">Issue Type</InputLabel>
						<Select
							labelId="demo-simple-select-label"
							id="demo-simple-select"
							value={filterValue}
							label="Issue Type"
							onChange={(e) => setFilterValue(e.target.value)}
						>
							<MenuItem value={"Infrastructure"}>Infrastructure</MenuItem>
							<MenuItem value={"Safety"}>Safety</MenuItem>
							<MenuItem value={"Public Health"}>Public Health</MenuItem>
							<MenuItem value={"Pollution"}>Pollution</MenuItem>
							<MenuItem value={"Disturbance"}>Disturbance</MenuItem>
							<MenuItem value={"Other"}>Other</MenuItem>
						</Select>
					</FormControl>
				</Box>
			);
		} else if (filterType === "dateposted" || filterType === "dateofissue") {
			inputDisplay = <TextField type="date" onChange={(e) => setFilterValue(e.target.value)}></TextField>;
		} else {
			inputDisplay = null;
		}
		return inputDisplay;
	}

	return (
		<Container>
			<Box fixed sx={{ mb: "4rem" }}>
				<Typography variant="h4">Issue Analysis</Typography>
				<Button onClick={getIssues}>Get All Issues</Button>
				<a href="http://35.232.3.28/ia/issues/report" target="_blank" rel="noreferrer noopener">
					<Button onClick={generateReport}>Generate Issue Report</Button>
				</a>
				<div></div>
				<div></div>
				<Box sx={{ minWidth: 120 }}>
					<FormControl fullWidth>
						<InputLabel id="demo-simple-select-label">Filter Issues By:</InputLabel>
						<Select
							labelId="demo-simple-select-label"
							id="demo-simple-select"
							value={filterType}
							label="Filter Issues"
							onChange={(e) => setFilterType(e.target.value)}
						>
							<MenuItem value={"type"}>Issue Type</MenuItem>
							<MenuItem value={"dateposted"}>Date Posted</MenuItem>
							<MenuItem value={"dateofissue"}>Date of Issue</MenuItem>
							<MenuItem value={"reviewed"}>Reviewed</MenuItem>
							<MenuItem value={"highlighted"}>Highligted</MenuItem>
						</Select>
					</FormControl>
				</Box>
				{inputDisplay()}
				<Button onClick={getFilteredIssues}>Get Filtered Issues</Button>

				<IssueTable issues={issues}></IssueTable>
			</Box>
		</Container>
	);
}
