import Table from "react-bootstrap/Table";
import Button from "@mui/material/Button";
import axios from "axios";
import Tooltip from "@mui/material/Tooltip";
import { useEffect, useState } from "react";

export default function IssueTable(props) {
	const issues = props.issues || [];
	const urlIA = "http://35.232.3.28/ia/issues";

	const [highUp, setHighUp] = useState(false);
	const [reviUp, setReviUp] = useState(false);

	useEffect(() => {}, [highUp, reviUp]);

	const tRows = issues.map((i) => (
		<tr key={i.id}>
			<td>{i.datePosted.substring(0, 10)}</td>
			<td>{i.dateOfIssue}</td>
			<td>{i.issueType}</td>
			<td>{i.issueDescription}</td>
			<td>{i.location}</td>
			<td>
				<Tooltip title="Click to Mark as Reviewed">
					<Button data-value={issues.indexOf(i)} onClick={reviewClick}>
						{i.reviewed ? "Reviewed" : "Not Reviewed"}
					</Button>
				</Tooltip>
			</td>
			<td>
				<Tooltip title="Click to Mark as Highlighted">
					<Button data-value={issues.indexOf(i)} onClick={highlightClick}>
						{i.highlighted ? "Highlighted" : "Not Highlighted"}
					</Button>
				</Tooltip>
			</td>
		</tr>
	));

	function highlightClick(e) {
		const issueIndex = e.target.getAttribute("data-value");
		const issueToUpdate = issues[issueIndex];
		highlightIssue(issueToUpdate);
	}

	function reviewClick(e) {
		const issueIndex = e.target.getAttribute("data-value");
		const issueToUpdate = issues[issueIndex];
		reviewIssue(issueToUpdate);
	}

	async function highlightIssue(i) {
		const issue = i;
		issue.highlighted ? (issue.highlighted = false) : (issue.highlighted = true);
		const response = await axios.put(`${urlIA}/${issue.id}`, issue);
		alert(response.data);
		highUp ? setHighUp(false) : setHighUp(true);
	}

	async function reviewIssue(i) {
		const issue = i;
		issue.reviewed = true;
		const response = await axios.put(`${urlIA}/${issue.id}`, issue);
		alert(response.data);
		reviUp ? setReviUp(false) : setReviUp(true);
	}

	return (
		<Table margin-bottom="4rem" size="sm" striped bordered hover>
			<thead>
				<tr>
					<th>Date Posted</th>
					<th>Date of Issue</th>
					<th>Type</th>
					<th>Description</th>
					<th>Location</th>
					<th>Reviewed</th>
					<th>Highlighted</th>
				</tr>
			</thead>
			<tbody>{tRows}</tbody>
		</Table>
	);
}
