import axios from "axios";

const BASE_URL = "http://35.232.3.28/au/auth";

export function login(data) {
	return axios
		.post(`${BASE_URL}`, { email: data.email, password: data.password })
		.then((response) => {
			localStorage.setItem("x-access-token", response.data.token);
			localStorage.setItem("x-access-token-expiration", Date.now() + 2 * 60 * 60 * 1000);
			return response.data;
		})
		.catch((err) => Promise.reject("Authentication Failed!"));
}

export function isAuthenticated() {
	return localStorage.getItem("x-access-token") && localStorage.getItem("x-access-token-expiration") > Date.now();
}
