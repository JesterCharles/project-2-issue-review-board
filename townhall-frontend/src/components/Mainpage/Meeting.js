import React, { Component } from "react";
import MeetingForm from "../MeetingComponents/meeting-form";
import MeetingView from "../MeetingComponents/meeting-view";
import { getRegularTips } from "./repository";

class Meeting extends Component {
	render() {
		return (
			<div>
				<h3 className="text-center">Town Meeting Scheduler</h3>
				<hr />
				<MeetingView />
			</div>
		);
	}
}

export default Meeting;
