import axios from "axios";
import React, { Component } from "react";

class Home extends Component {
	render() {
		return (
			<div>
				<hr />
				<h1 className="text-center">Welcome to Not Sure Town Home Page</h1>
				<div className="">
					<h6 className="text-center">
						Located in the heart of the Czech Republic, <b>Not Sure Town</b> is rated by Traveler Magazine as{" "}
						<b>“one of the best towns in the world for architecture lovers.”</b>{" "}
					</h6>
				</div>
				<hr />
				<img class="img-fluid rounded mb-4 mb-lg-0" src="https://images.alphacoders.com/743/743285.jpg" alt="" />
				);
			</div>
		);
	}
}

export default Home;
