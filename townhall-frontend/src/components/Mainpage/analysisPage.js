import React, { Component } from "react";
import { getSpecialTips, isAuthenticated } from "./repository";
import { Redirect } from "react-router-dom";

class AnalysisPage extends Component {
	constructor() {
		super();
		this.state = { auth: true };
	}

	componentDidMount() {
		if (isAuthenticated()) {
		} else {
			alert("User Not Authenticated");
			this.setState({ auth: false });
			window.location = "https://townhall-group5.web.app/";
		}
	}

	render() {
		return (
			<div>
				{this.state.auth ? "" : <Redirect to="/analysis" />}
				<h3 className="text-center">All Town Issues: Council Members View</h3>
				<hr />
				{/* <img class="img-fluid rounded mb-4 mb-lg-0" src="https://wallpaperaccess.com/full/245672.jpg" alt="" /> */}
			</div>
		);
	}
}
export default AnalysisPage;
