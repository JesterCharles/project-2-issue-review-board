import { createEmitAndSemanticDiagnosticsBuilderProgram } from "typescript";
import { client } from "../src/connection";
import { MeetingDAO } from "../src/daos/meeting-dao";
import MeetingDAOImpl from "../src/daos/meeting-dao-postgres-impl";
import { Meeting } from "../src/entities";

const meetingDAO: MeetingDAO = new MeetingDAOImpl();

test("Create wedding by ID", async () => {
	const testMeeting: Meeting = new Meeting(0, "NotSure Community Library", true, false, false, false, true, false, "", "Rafael Mora");
	const result = await meetingDAO.createMeeting(testMeeting);

	expect(result.meetingId).toBeGreaterThan(0);
});
test("Obtain all weddings", async () => {
	const testMeeting1: Meeting = new Meeting(0, "NotSure Community Park", true, false, false, false, true, false, "", "Josh Forcier");
	const testMeeting2: Meeting = new Meeting(
		0,
		"NotSure Community Library",
		true,
		false,
		false,
		false,
		true,
		false,
		"",
		"Michelle Pierce"
	);
	const testMeeting3: Meeting = new Meeting(
		0,
		"NotSure High School",
		true,
		false,
		true,
		false,
		true,
		true,
		"Pigeon Pandemic",
		"Charles Jester"
	);
	await meetingDAO.createMeeting(testMeeting1);
	await meetingDAO.createMeeting(testMeeting2);
	await meetingDAO.createMeeting(testMeeting3);

	const result: Meeting[] = await meetingDAO.allMeetings();
	expect(result.length).toBeGreaterThanOrEqual(3);
});
test("Obtain wedding by ID", async () => {
	const testMeeting: Meeting = new Meeting(0, "NotSure Community Center", true, false, true, false, true, false, "", "Adam Ranieri");
	const result: Meeting = await meetingDAO.createMeeting(testMeeting);
	const result2: Meeting = await meetingDAO.meetingByID(result.meetingId);

	expect(result2.meetingId).toBe(result.meetingId);
});
test("Update meeting by ID", async () => {
	const testMeeting: Meeting = new Meeting(
		0,
		"NotSure Town Hall Auditorium",
		true,
		false,
		false,
		false,
		true,
		true,
		"Issue reporting",
		"Henry Hsieh"
	);
	const result: Meeting = await meetingDAO.createMeeting(testMeeting);

	const updatedMeeting: Meeting = new Meeting(
		result.meetingId,
		"NotSure Town Hall Auditorium",
		true,
		true,
		true,
		true,
		true,
		true,
		"Aliens in the skies",
		"Henry Hsieh"
	);
	const update: Meeting = await meetingDAO.updateMeeting(updatedMeeting);
	expect(updatedMeeting.otherValue).toBe(update.otherValue);
});
test("Delete wedding meeting by ID", async () => {
	const testMeeting: Meeting = new Meeting(0, "NotSure Community Library", true, false, false, false, true, false, "", "Donavan Merrin");
	const result = await meetingDAO.createMeeting(testMeeting);

	const del = await meetingDAO.deleteMeeting(result.meetingId);
	expect(del).toBeTruthy;
});

afterAll(async () => {
	await client.end();
});
