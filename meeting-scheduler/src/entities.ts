export class Meeting {
	constructor(
        public meetingId: number, 
        public location: string, 
        public safety: boolean, 
        public Infrastructure: boolean, 
        public publicHealth: boolean, 
        public pollution: boolean, 
        public disturbance: boolean, 
        public other: boolean, 
        public otherValue: string, 
        public councilLead: string
        ) {}
}
