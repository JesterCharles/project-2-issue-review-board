import { Meeting } from "../entities";
import { MeetingDAO } from "./meeting-dao";
import { client } from "../connection";
import { MissingResourceError } from "../errors";

export default class MeetingDAOImpl implements MeetingDAO {
	async createMeeting(meeting: Meeting): Promise<Meeting> {
		const sql: string =
			"insert into meeting(meeting_location, meeting_safety, meeting_infrastructure, meeting_public_health, meeting_pollution, meeting_disturbance, meeting_other, meeting_other_value, meeting_council_lead) values ($1,$2,$3,$4,$5,$6,$7,$8,$9) returning meeting_id";
		const values = [
			meeting.location,
			meeting.safety,
			meeting.Infrastructure,
			meeting.publicHealth,
			meeting.pollution,
			meeting.disturbance,
			meeting.other,
			meeting.otherValue,
			meeting.councilLead
		];
		const result = await client.query(sql, values);
		meeting.meetingId = result.rows[0].meeting_id;
		return meeting;
	}

	async allMeetings(): Promise<Meeting[]> {
		const sql: string = "select * from meeting";
		const result = await client.query(sql);
		const meetings: Meeting[] = [];
		for (const row of result.rows) {
			const meeting: Meeting = new Meeting(
				row.meeting_id,
				row.meeting_location,
				row.meeting_safety,
				row.meeting_infrastructure,
				row.meeting_public_health,
				row.meeting_pollution,
				row.meeting_disturbance,
				row.meeting_other,
				row.meeting_other_value,
				row.meeting_council_lead
			);
			meetings.push(meeting);
		}
		return meetings;
	}

	async meetingByID(meetingId: number): Promise<Meeting> {
		const sql: string = "select * from meeting where meeting_id = $1";
		const values = [meetingId];
		const result = await client.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The meeting with id ${meetingId} does not exist`);
		}
		const row = result.rows[0];
		const meeting: Meeting = new Meeting(
			row.meeting_id,
			row.meeting_location,
			row.meeting_safety,
			row.meeting_infrastructure,
			row.meeting_public_health,
			row.meeting_pollution,
			row.meeting_disturbance,
			row.meeting_other,
			row.meeting_other_value,
			row.meeting_council_lead
		);
		return meeting;
	}

	async updateMeeting(meeting: Meeting): Promise<Meeting> {
		const sql: string =
			"update meeting set meeting_id=$1, meeting_location=$2, meeting_safety=$3, meeting_infrastructure=$4, meeting_public_health=$5, meeting_pollution=$6, meeting_disturbance=$7, meeting_other=$8, meeting_other_value=$9, meeting_council_lead=$10 where meeting_id=$11";
		const values = [
			meeting.meetingId,
			meeting.location,
			meeting.safety,
			meeting.Infrastructure,
			meeting.publicHealth,
			meeting.pollution,
			meeting.disturbance,
			meeting.other,
			meeting.otherValue,
			meeting.councilLead,
			meeting.meetingId
		];
		const result = await client.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The meeting with id ${meeting.meetingId} does not exist`);
		}
		return meeting;
	}

	async deleteMeeting(meetingId: number): Promise<Boolean> {
		const sql: string = "delete from meeting where meeting_id=$1";
		const values = [meetingId];
		const result = await client.query(sql, values);
		return true;
	}
}
