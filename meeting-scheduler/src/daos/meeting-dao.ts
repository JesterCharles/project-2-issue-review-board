import { Meeting } from "../entities";

export interface MeetingDAO {
	// Create
	createMeeting(meeting: Meeting): Promise<Meeting>;

	// Read
	allMeetings(): Promise<Meeting[]>;
	meetingByID(meetingId: number): Promise<Meeting>;

	// Update
	updateMeeting(meeting: Meeting): Promise<Meeting>;

	// Delete
	deleteMeeting(meetingId: number): Promise<Boolean>;
}
