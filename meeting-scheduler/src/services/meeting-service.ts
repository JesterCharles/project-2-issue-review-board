import { Meeting } from "../entities";

export interface MeetingService {
	// Create
	scheduleMeeting(meeting: Meeting): Promise<Meeting>;

	// Read
	allMeetings(): Promise<Meeting[]>;
	meetingByID(meetingId: number): Promise<Meeting>;

	// Update
	changeMeeting(meeting: Meeting): Promise<Meeting>;

	// Delete
	cancelMeeting(meetingId: number): Promise<Boolean>;
}
