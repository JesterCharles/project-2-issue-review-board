import { Meeting } from "../entities";
import { MeetingService } from "./meeting-service";
import { client } from "../connection";
import { MeetingDAO } from "../daos/meeting-dao";
import MeetingDAOImpl from "../daos/meeting-dao-postgres-impl";

export default class MeetingServiceImpl implements MeetingService {
	meetingDAO: MeetingDAO = new MeetingDAOImpl();
	scheduleMeeting(meeting: Meeting): Promise<Meeting> {
		return this.meetingDAO.createMeeting(meeting);
	}
	allMeetings(): Promise<Meeting[]> {
		return this.meetingDAO.allMeetings();
	}
	meetingByID(meetingId: number): Promise<Meeting> {
		return this.meetingDAO.meetingByID(meetingId);
	}
	changeMeeting(meeting: Meeting): Promise<Meeting> {
		return this.meetingDAO.updateMeeting(meeting);
	}
	cancelMeeting(meetingId: number): Promise<Boolean> {
		return this.meetingDAO.deleteMeeting(meetingId);
	}
}
